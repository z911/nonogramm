/*
// nonogramme ver01 
// vlad zolo (dec 2021)
*/

class MyTimer {
    static displayName = "Timer";
    constructor(initTime = 0) {
        this._currentTime = initTime;
        this._myInterval = undefined
    }
    startTimer() {
        this.stopTimer();
        this.resetTime();
        this._myInterval = setInterval( () =>{
            this._currentTime++
            this.updateAffichageTimer()
        }, 1000)
    }
    stopTimer() {
        clearInterval(this._myInterval);
    }
    resetTime() {
        this._currentTime = 0;
        this.updateAffichageTimer()
    }
    updateAffichageTimer() {
        document.getElementById('timer').innerHTML='Timer : ' + this._currentTime + ' sec.';
    }
}

let appTimer = new MyTimer(0)
let isStatusAppPlay = true 

function change_backgraund(ev) {
    ev.target.classList.toggle('selected_class')
}

function createSequencesChiffresJson(cellsRows) { 
    const newData = []
    let a = undefined
    cellsRows.forEach( line => {
        let row = []
        a = 0
        line.forEach( cell => {
            if ( cell  && (a === 0) ){ a = 1 
            } else if (cell){  a++     
            } else if (!cell){          
                if ( a > 0) {                  
                    row.push(a)
                    a = 0
                }
            }
        })
        if ( a > 0) { row.push(a) } 
        newData.push(row)
    })
    return newData
}

function afficherNewJson(Title, cellsRows, cellsColumns) { 
    const newData={ 
        "title": Title,
        "data": {
            "rows" : createSequencesChiffresJson(cellsRows),
            "columns" : createSequencesChiffresJson(cellsColumns)
            },
        "size": {"rows": cellsRows.length,"columns": cellsColumns.length}
        }
    return newData
}

function inverseArray(myArray){
    // create an empty "newArray": 
    // the number of rows (empty arrays) is equal to the number of columns in "myArray"
    let newArray= []
    for (let init = 0; init < myArray[0].length; init++){
        newArray.push([])
    }
    // put the data from "myArray" into a "newArray":
    // Values ​​in cell [i,j] put into cell [j,i]
    for (let i = 0; i < myArray.length; i++){
        const ln = myArray[i]
        for (let j=0; j<ln.length; j++){
            newArray[j].push(ln[j])
        }
    }
    return newArray
}

function genererTableFromJson(ev){
    isStatusAppPlay = true 
    console.log(' Here function genererTableFromJson for nono :', ev.target)
    const monNono = {"title":"OOO",
    "data":{"rows":[[2],[1,1],[1,1],[2]],"columns":[[2],[1,1],[1,1],[2]]},
    "size":{"rows":4,"columns":4}}
    appTimer.startTimer() // timer
    const monData = monNono.data // for victory

    if (document.querySelector('table')){
        document.querySelector('table').remove()
    }
    document.getElementById('export-container').innerHTML=''

    if (document.querySelector('h3')){
        document.querySelector('h3').remove()
    }
    const formTable = document.getElementById('formTable')
    const ln = monNono.size.rows + 1
    const col = monNono.size.columns + 1

    const body = document.body
    let table= document.createElement('table')

    for (let i = 0; i < ln; i++){
        let tr_line = document.createElement('tr') 
        for (let j = 0; j < col; j++){
            let td_col = document.createElement('td') 
            if ( i === 0 && j > 0) {
                td_col.innerHTML=''+ monData.rows[j-1].join(' ')
                td_col.classList.add('tdheadRow');
            } else if (j === 0 && i > 0) {
                td_col.innerHTML=''+ monData.columns[i-1].join('&nbsp;')
                td_col.classList.add('tdheadCol')
            } else {
                td_col.classList.add('tdstyle');
            
                td_col.addEventListener('click',(ev)=>{
                    ev.target.classList.toggle('selected_class')

                    //CALCULE JSON
                    let userData = []
                    let isTrFirst = true
                    const trs = document.querySelectorAll('tr')
                    trs.forEach(tr => {
                        if (isTrFirst) { 
                            isTrFirst= false
                        } else {
                            let str = []
                            //
                            // const [, ...cells] = tr.cells
                            // for (const cell of cells){
                            //
                            let isCellFirst = true
                            for (const cell of tr.cells){
                                if (isCellFirst) { 
                                    isCellFirst= false
                                } else {
                                    str.push(cell.classList.contains('selected_class'))
                                }
                            }
                            userData.push(str)
                        }
                    })
                    const  userDataJson = afficherNewJson('nameTitle', userData, inverseArray(userData)).data
                    // console.log(monData)
                    // console.log(userDataJson)
                    if (JSON.stringify(monData) === JSON.stringify(userDataJson)) {
                        appTimer.stopTimer()
                        console.log('Victory!!')  
                        let headerVictory = document.createElement('h3') 
                        headerVictory.innerText='Victoire !!'
                        body.appendChild(headerVictory) 
                        
                    } else {
                        console.log('Pas encore...') 
                    }
                })
            }
            tr_line.appendChild(td_col)
        }
        table.appendChild(tr_line)
    }
    body.appendChild(table) 

}

function createListNono(dataArray){

    if (document.querySelector('ul')){
        document.querySelector('ul').remove();
    }
    if (document.querySelector('h2')){
        document.querySelector('h2').remove();
    }

    const nodeImport = document.querySelector('#import-container')
    
    let header= document.createElement('h2')
    header.innerText='Liste des nonogramme:'
    nodeImport.appendChild(header)

    let ul= document.createElement('ul')
    nodeImport.appendChild(ul)

    dataArray.forEach( el =>{
        let li= document.createElement('li')
        li.innerText=`${el.title} (${el.size.rows} x ${el.size.columns})`
        li.addEventListener('click', () =>{
            const monNono = el
            document.getElementById('export-container').innerHTML=''
            
            appTimer.startTimer() // timer
            const monData = monNono.data // for victory

            if (document.querySelector('table')){
                document.querySelector('table').remove();
            }
            if (document.querySelector('h3')){
                document.querySelector('h3').remove();
            }
            const formTable = document.getElementById('formTable')
            const ln = monNono.size.rows + 1
            const col = monNono.size.columns + 1
            // console.log('ln ',ln,'col ',col)
            const body = document.body
            let table= document.createElement('table')

            for (let i = 0; i < ln; i++){
                let tr_line = document.createElement('tr') 
                for (let j = 0; j < col; j++){
                    let td_col = document.createElement('td') 
                    if ( i === 0 && j > 0) {
                        td_col.innerHTML=''+ monData.columns[j-1].join(' ')
                        td_col.classList.add('tdheadRow');
                    } else if (j === 0 && i > 0) {
                        td_col.innerHTML=''+ monData.rows[i-1].join('&nbsp;')
                        td_col.classList.add('tdheadCol')
                    } else {
                        td_col.classList.add('tdstyle');
                    
                        td_col.addEventListener('click',(ev)=>{
                            ev.target.classList.toggle('selected_class')

                            //CALCULE JSON
                            let userData = []
                            let isTrFirst = true
                            const trs = document.querySelectorAll('tr')
                            trs.forEach(tr => {
                                if (isTrFirst) { 
                                    isTrFirst= false
                                } else {
                                    let str = []
                                    //
                                    // const [, ...cells] = tr.cells
                                    // for (const cell of cells){
                                    //
                                    let isCellFirst = true
                                    for (const cell of tr.cells){
                                        if (isCellFirst) { 
                                            isCellFirst= false
                                        } else {
                                            str.push(cell.classList.contains('selected_class'))
                                        }
                                    }
                                    userData.push(str)
                                }
                            })
                            const  userDataJson = afficherNewJson('nameTitle', userData, inverseArray(userData)).data
                            // console.log(monData)
                            // console.log(userDataJson)
                            if (JSON.stringify(monData) === JSON.stringify(userDataJson)) {
                                appTimer.stopTimer()
                                console.log('Victory!!')  
                                let headerVictory = document.createElement('h3') 
                                headerVictory.innerText='Victoire !!'
                                body.appendChild(headerVictory) 
                                
                            } else {
                                console.log('Pas encore...') 
                            }
                        })
                    }
                    tr_line.appendChild(td_col)
                }
                table.appendChild(tr_line)
            }
            body.appendChild(table) 
        } )
        ul.appendChild(li)

    })
}

function importNonoFromDataJson(){
    document.getElementById('export-container').innerHTML=''
    fetch('data.json')
        .then(response => response.json())
        .then(data => {
            console.log(data)
            createListNono(data)
        })
        .catch(err => console.log(`Error: Quelque chose s'est mal passé... (${err}) `));
}

function createCodeJson(){
    if (isStatusAppPlay) {
        console.log(`Error: Il faut d'abord créer la table !`)
        return 'error'
    }
    try{
        const formTable = document.getElementById('formTable')
        const nameTitle = formTable.nono.value
        let res = {
            title: nameTitle,
            data: []
        }
        const trs = document.querySelectorAll('tr')
    
        trs.forEach(tr => {
            let str = []
            for (const cell of tr.cells){
                str.push(cell.classList.contains('selected_class'))
            }
            res.data.push(str)
        })

        const cellsStateByRow = res.data
        const cellsStateByColumn = inverseArray(cellsStateByRow)
        const newDataList = JSON.stringify(afficherNewJson(nameTitle, cellsStateByRow, cellsStateByColumn))
    
        const nodeExportContainer = document.querySelector(`#export-container`)
        nodeExportContainer.innerHTML = (newDataList)
    } 
    catch (err){
        console.log(`Error: Il faut d'abord créer la table ! (${err}) `)
    }
    
}

function createNewTable() {
 isStatusAppPlay = false
 document.getElementById('export-container').innerHTML=''

    // delete table
    if (document.querySelector('table')){
        document.querySelector('table').remove();
    }
    const formTable = document.getElementById('formTable')
    // console.log('formTable button ...')
    const ln = formTable.lines.value
    const col = formTable.column.value
    // console.log('',ln,' x ',col)
    const body = document.body
    // for lines
    let table= document.createElement('table')
    for (let i = 0; i < ln; i++){
        let tr_line = document.createElement('tr') 
        // for columns
        for (let j = 0; j < col; j++){
            let td_col = document.createElement('td') 
            td_col.classList.add('tdstyle');
            td_col.addEventListener('click',change_backgraund) // add 'Listener' 
            tr_line.appendChild(td_col)
        }
        table.appendChild(tr_line)
    }
    body.appendChild(table) 
}

function init(ev){
    formTable.CreateTable.addEventListener('click', createNewTable)
    formTable.createJson.addEventListener('click', createCodeJson)
    formTable.importNono.addEventListener('click', importNonoFromDataJson)
}

window.addEventListener('load', init)